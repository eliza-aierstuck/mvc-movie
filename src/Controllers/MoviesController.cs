using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MvcMovie.Models;
using MvcMovie.Repositories;

namespace MvcMovie.Controllers
{
    public class MoviesController : Controller
    {
        private readonly IGenericRepository<Movie> movieRepository;

        public MoviesController(IGenericRepository<Movie> movieRepository)
        {
            this.movieRepository = movieRepository;
        }

        // GET: Movies
        public async Task<ViewResult> Index(string movieGenre, string searchString)
        {
            IEnumerable<Movie> movies;
            if (searchString != null)
            {
               movies = await movieRepository.GetAsync(x => x.Title.Contains(searchString));
            }
            else
            {
                movies = await movieRepository.GetAsync();
            }
            
            var moviesList = movies.ToList();
            
            IQueryable<string> genreQuery = from m in moviesList.AsQueryable()
                orderby m.Genre
                select m.Genre;
            var genres = genreQuery.Distinct().ToList();

            var movieGenreVm = new MovieGenreViewModel
            {
                Genres = new SelectList(genres),
                Movies = moviesList
            };

            return View(movieGenreVm);
        }

        // GET: Movies/Details/5
        public IActionResult Details(int? id)
        {
            var movie = movieRepository.GetById(id);
            if (movie == null)
            {
                return NotFound();
            }

            return View(movie);
        }

        // GET: Movies/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Movies/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create([Bind("Id,Title,ReleaseDate,Genre,Price")]
            Movie movie)
        {
            if (ModelState.IsValid)
            {
                movieRepository.Insert(movie);
                return RedirectToAction(nameof(Index));
            }

            return View(movie);
        }

        // GET: Movies/Edit/5
        public IActionResult Edit(int? id)
        {
            var movie = movieRepository.GetById(id);
            if (movie == null)
            {
                return NotFound();
            }

            return View(movie);
        }

        // POST: Movies/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(int id, [Bind("Id,Title,ReleaseDate,Genre,Price")]
            Movie movie)
        {
            if (id != movie.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    movieRepository.Update(movie);
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!MovieExists(movie.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }

                return RedirectToAction(nameof(Index));
            }

            return View(movie);
        }

        // GET: Movies/Delete/5
        public IActionResult Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var movie = movieRepository.GetById(id);
            if (movie == null)
            {
                return NotFound();
            }

            return View(movie);
        }

        // POST: Movies/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public IActionResult DeleteConfirmed(int id)
        {
            var movie = movieRepository.GetById(id);
            movieRepository.Delete(movie);
            return RedirectToAction(nameof(Index));
        }

        private bool MovieExists(int id)
        {
            return (movieRepository.GetById(id) == null);
        }
    }
}