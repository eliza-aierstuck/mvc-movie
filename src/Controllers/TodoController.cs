using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MvcMovie.Models;
using MvcMovie.Repositories;

namespace MvcMovie.Controllers
{
  [Route("api/todo")]
  [ApiController]
  public class TodoController : ControllerBase
  {
    private readonly IGenericRepository<TodoItem> _repository;

    public TodoController(IGenericRepository<TodoItem> repository)
    {
      this._repository = repository;

      if (_repository.Get().Any()) return;
      
      _repository.Insert(new TodoItem { Name = "Item1"});
    }

    // GET: api/Todo
    [HttpGet]
    public async Task<IEnumerable<TodoItem>> GetTodoItems()
    {
      return await _repository.GetAsync();
    }

    // GET: api/Todo/5
    [HttpGet("{id}")]
    public async Task<ActionResult<TodoItem>> GetTodoItem(long id)
    {
      var todoItem = await _repository.GetByIdAsync(id);

      if (todoItem == null)
      {
        return NotFound();
      }

      return todoItem;
    }

  }
}