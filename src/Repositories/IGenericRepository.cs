using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace MvcMovie.Repositories
{
  public interface IGenericRepository<TEntity>
  {
    Task<IEnumerable<TEntity>> GetAsync(Expression<Func<TEntity, bool>> filter = null,
      Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
      string includeProperties = "");

    Task<IEnumerable<TEntity>> GetAsync();
    IEnumerable<TEntity> Get();
    
    TEntity GetById(object id);
    
    Task<TEntity> GetByIdAsync(object id);

    void Insert(TEntity item);

    void Delete(object id);

    void Delete(TEntity entity);

    void Update(TEntity entity);
  }
}