using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DeepEqual.Syntax;
using Microsoft.AspNetCore.Mvc;
using Moq;
using MvcMovie.Controllers;
using MvcMovie.Models;
using MvcMovie.Repositories;
using Xunit;

namespace test.UnitTests.Controllers
{
  public class TodoControllerTests
  {
    private TodoController _controller;
    private Mock<IGenericRepository<TodoItem>> mockRepo;
    

    public TodoControllerTests()
    {
      this.mockRepo = new Mock<IGenericRepository<TodoItem>>();
      this._controller = new TodoController(mockRepo.Object);
    }
    
    [Fact]
    public async Task GetTodoItems_ReturnsListOfActionItems()
    {
      mockRepo.Setup(mock => mock.GetAsync()).ReturnsAsync(TestTodoItems());

      var result = await _controller.GetTodoItems();
      var list = Assert.IsType<List<TodoItem>>(result);
      list.ShouldDeepEqual(TestTodoItems());
    }

    [Fact]
    public async Task GetTodoItem_ReturnsTodoItemWithRequestedId()
    {
      mockRepo.Setup(mock => mock.GetByIdAsync(1)).ReturnsAsync(TestTodoItems().First());

      var result = await _controller.GetTodoItem(1);
      Assert.IsType<ActionResult<TodoItem>>(result);
    }
    
    private static List<TodoItem> TestTodoItems()
    {
      var todoItems = new List<TodoItem>();
      todoItems.Add(new TodoItem()
      {
        Id = 1,
        Name = "TODO One"
      });
      todoItems.Add(new TodoItem()
      {
        Id = 2,
        Name = "TODO Two"
      });
      return todoItems;
    }
  }
}