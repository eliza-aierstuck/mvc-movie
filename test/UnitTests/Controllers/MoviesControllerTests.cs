using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Moq;
using MvcMovie.Controllers;
using MvcMovie.Models;
using MvcMovie.Repositories;
using Xunit;

namespace test.UnitTests.Controllers
{
  public class MoviesControllerTests
  {
    private readonly Mock<IGenericRepository<Movie>> _mockRepo;
    private readonly MoviesController _controller;

    public MoviesControllerTests()
    {
      _mockRepo = new Mock<IGenericRepository<Movie>>();
      _controller = new MoviesController(_mockRepo.Object);     
    }

    [Fact]
    public void Edit_UpdatesMovieAndRedirectsToIndex()
    {
      var movie = new Movie()
      {
        Id = 1,
        Title = "updated Title"
      };
      var result = _controller.Edit(1, movie);
      ;
      _mockRepo.Verify(mock => mock.Update(movie));
      var redirectToActionResult = Assert.IsType<RedirectToActionResult>(result);
      Assert.Equal("Index", redirectToActionResult.ActionName);
    }

    [Fact]
    public async Task Index_ReturnsViewResult_WithListOfMovies()
    {
      _mockRepo.Setup(repo => repo.GetAsync())
        .ReturnsAsync(TestMovies());

      var result = await _controller.Index(null, null);

      var viewResult = Assert.IsType<ViewResult>(result);
      var moviesVm = Assert.IsAssignableFrom<MovieGenreViewModel>(viewResult.ViewData.Model);
      Assert.Equal(2, moviesVm.Movies.Count());
    }

    [Fact]
    public void Details_ReturnsViewResultOfMovieWithGivenId() 
    {
      var movie = TestMovies().First();
      _mockRepo.Setup(repo => repo.GetById(movie.Id))
        .Returns(movie);

      var result = _controller.Details(movie.Id);

      var viewResult = Assert.IsType<ViewResult>(result);
      var returnedMovie = Assert.IsType<Movie>(viewResult.ViewData.Model);
      Assert.Equal(movie, returnedMovie);
    }

    [Fact]
    public void Details_ReturnsNotFound_WhenMovieNotFound()
    {
      _mockRepo.Setup(repo => repo.GetById(null))
        .Returns((Movie) null);
      var result = _controller.Details(null);
      Assert.IsType<NotFoundResult>(result);
    } 

    [Fact]
    public void Create_SavesMovie_AndRedirectsToIndex()
    {
      Movie newMovie = new Movie()
      {
        Title = "New Movie"
      };

      var result = _controller.Create(newMovie);

      _mockRepo.Verify(m => m.Insert(newMovie));
      var redirectToActionResult = Assert.IsType<RedirectToActionResult>(result);
      Assert.Equal("Index", redirectToActionResult.ActionName);
    }

    [Fact]
    public void Create_DoesNotSaveMovie_OrRedirect_WhenMovieIsInvalid()
    {
      Movie invalidMovie = new Movie();
      _controller.ModelState.AddModelError("test", "test");

      var result = _controller.Create(invalidMovie);

      _mockRepo.VerifyNoOtherCalls();
      ViewResult viewResult = Assert.IsType<ViewResult>(result);
      Movie movie = Assert.IsType<Movie>(viewResult.ViewData.Model);
      Assert.Equal(invalidMovie, movie);
    }

    private List<Movie> TestMovies()
    {
      var movies = new List<Movie>();
      movies.Add(new Movie()
      {
        Id = 1,
        Title = "Movie One"
      });
      movies.Add(new Movie()
      {
        Id = 2,
        Title = "Movie Two"
      });
      return movies;
    }
  }
}