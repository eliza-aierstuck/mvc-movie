using System.Linq;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Hosting.Server.Features;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.AspNetCore.TestHost;

namespace test.JourneyTests
{
  public class SeleniumServerFactory<TStartup> : WebApplicationFactory<TStartup> where TStartup : class
  {
    public string RootUri { get; set; }

    private IWebHost host;

    protected override TestServer CreateServer(IWebHostBuilder builder)
    { 
      host = builder.UseUrls("http://localhost:5050").Build();
      host.Start();
      RootUri = host.ServerFeatures.Get<IServerAddressesFeature>().Addresses.LastOrDefault();
      
      return new TestServer(new WebHostBuilder().UseStartup<TStartup>());
    }

    protected override void Dispose(bool disposing)
    {
      base.Dispose(disposing);
      if (disposing) {
        host.Dispose();
      }
    }
  }
}