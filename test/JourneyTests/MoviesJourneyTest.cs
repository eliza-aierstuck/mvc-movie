using System;
using MvcMovie;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Remote;
using Xunit;

namespace test.JourneyTests
{
  public class MoviesJourneyTest : IDisposable, IClassFixture<SeleniumServerFactory<Startup>>
  {
    private readonly IWebDriver browser;
    private readonly string rootUri;
    
    public MoviesJourneyTest(SeleniumServerFactory<Startup> server)
    {
      server.CreateClient();
      rootUri = server.RootUri;
      
      ChromeOptions options = new ChromeOptions();
      options.AddArgument("headless");
      options.AddArgument("--window-size=1920,1080");
      browser = new RemoteWebDriver(options);
    }

    [Fact]
    public void MoviesCrud()
    {
      //start on movies index
      browser.Navigate().GoToUrl(rootUri);
      Assert.Equal("Index - Movies!", browser.Title);
      
      //attempt to create new invalid movie
      browser.FindElement(By.LinkText("Create New")).Click();
      Assert.Equal("Create - Movies!", browser.Title);
      browser.ClickSubmitButton();
    }

    public void Dispose()
    {
      try
      {
        browser.Quit();
      }
      catch (Exception e)
      {
        Console.WriteLine("error while stopping chrome: ", e);
      }
    }
  }
}