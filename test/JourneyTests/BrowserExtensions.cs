using OpenQA.Selenium;

namespace test.JourneyTests
{
  public static class BrowserExtensions
  {
    public static void ClickSubmitButton(this IWebDriver browser)
    {
      browser.FindElement(By.CssSelector("[type=\"submit\"]")).Click();
    }
  }
}