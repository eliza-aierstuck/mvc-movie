using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using AngleSharp.Html.Parser;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using MvcMovie;
using MvcMovie.Models;
using Xunit;

namespace test.IntegrationTests
{
  public class MoviesApiTests : IClassFixture<BaseWebApplicationFactory<Startup>>
  {
    private readonly HttpClient _client;
    private Movie _existingMovie;

    public MoviesApiTests(BaseWebApplicationFactory<Startup> factory)
    {
      _client = factory.WithWebHostBuilder(config =>
      {
        config.ConfigureServices((services =>
        {
          using (var scope = services.BuildServiceProvider().CreateScope())
          {
            var scopedServices = scope.ServiceProvider;
            var db = scopedServices.GetRequiredService<MvcMovieContext>();
            var logger = scopedServices.GetRequiredService<ILogger<MoviesApiTests>>();

            try
            {
              db.Movie.AddRange(GetSeedingMovies());
              db.SaveChanges();
            }
            catch (Exception e)
            {
              logger.LogError("errored on seeding ", e.Message);
            }

            _existingMovie = db.Movie.First();
          }
        }));
      }).CreateClient();
    }

    [Theory]
    [InlineData("/movies")]
    [InlineData("/movies/details/1")]
    [InlineData("/movies/create")]
    [InlineData("/movies/edit/1")]
    [InlineData("/movies/delete/1")]
    public async Task CanHitAllGetEndpoints(string url)
    {
      var response = await _client.GetAsync(url);

      response.EnsureSuccessStatusCode();
      Assert.Contains("text/html", response.Content.Headers.ToString());
    }

    [Fact]
    public async Task CanAddNewMovie()
    {
      var requestContent = new Dictionary<string, string>()
      {
        {"Title", "My Title"},
        {"ReleaseDate", "2009-01-01"},
        {"Genre", "My Genre"},
        {"Price", "12.00"},
      };

      var response = await RequestHelpers.PostWithAntiForgeryToken(_client, "/movies/create", requestContent);
      response.EnsureSuccessStatusCode();

      var content = await response.Content.ReadAsStringAsync();
      var document = await new HtmlParser().ParseDocumentAsync(content);
      var title = document.QuerySelectorAll("table.table > tbody > tr > td:first-of-type")
        .Select(e => e.InnerHtml).ToList().Last();

      Assert.Contains("My Title", title);
    }

    [Fact]
    public async Task CanUpdateExistingMovie()
    {
      var requestContent = new Dictionary<string, string>()
      {
        {"Id", _existingMovie.Id.ToString()},
        {"Title", "New Title"},
        {"ReleaseDate", "2009-01-01"},
      };

      var response =
        await RequestHelpers.PostWithAntiForgeryToken(_client, $"/movies/edit/{_existingMovie.Id}", requestContent);
      response.EnsureSuccessStatusCode();
    }

    private static List<Movie> GetSeedingMovies()
    {
      var movies = new List<Movie>();
      movies.Add(new Movie()
      {
        Title = "Movie One"
      });
      movies.Add(new Movie()
      {
        Title = "Movie Two"
      });
      return movies;
    }
  }
}