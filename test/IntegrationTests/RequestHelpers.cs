using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace test.IntegrationTests
{
  public class RequestHelpers
  {
    public static async Task<HttpResponseMessage> PostWithAntiForgeryToken(HttpClient client, string requestUri, IDictionary<string, string> content)
    {
      HttpResponseMessage formResponse = await client.GetAsync(requestUri);
      var token = await AntiForgeryHelper.ExtractAntiForgeryToken(formResponse);
      
      content.Add("__RequestVerificationToken", token);
      
      var request = new HttpRequestMessage(HttpMethod.Post, requestUri)
      {
        Content = new FormUrlEncodedContent(content)
      };
      
      return await client.SendAsync(request);
    }
  }
}