#!/usr/bin/env bash

nohup java -jar selenium-server-standalone-3.141.59.jar &
pid=$!
echo $pid
dotnet test --filter Journey
kill -9 $pid